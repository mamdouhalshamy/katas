package com.innozones.mms;

public class RomanToArabic {

    public static int convertRomanNumberToArabicNumber(String number) {
        // I need a way to convert roman numbers into units tens hundreds thousands
        // XIV
        int arabicNumber = 0;
        String[] digits = number.split("");
        for (int i = digits.length - 1; i >= 0; i--) {
            int value = getNumericalValue(digits[i]);
            //  I II IV VI
            if (value == 1 || value == 10 || value == 100 || value == 1000) {
                // I
                if(digits.length == 1)
                    return value;
                // I(I) V(I) X(I)
                else if (isFirstDigit(i, digits.length))
                    arabicNumber += value;
                // X(I)V V(I)I I(I)I
                else if (isInBetween(i, digits.length))
                    if (isPreviousValueLessOrEqual(value, i, digits))
                        arabicNumber += value;
                    else
                        arabicNumber -= value;
                // (I)V (I)I
                else if (isLastDigit(i))
                    if (isPreviousValueLessOrEqual(value, i, digits))
                        arabicNumber += value;
                    else
                        arabicNumber -= value;
            } else
                arabicNumber += value;
        }
        return arabicNumber;
    }

    public static boolean isPreviousValueLessOrEqual(int value, int index, String[] digits){
        if(getNumericalValue(digits[index+1]) <= value)
            return true;
        else
            return false;
    }

    public static boolean isPreviousValueOne(int index, String[] digits) {
        if (getNumericalValue(digits[index + 1]) == 1)
            return true;
        else return false;
    }

    public static boolean isInBetween(int index, int length) {
        if (!isLastDigit(index) && !isFirstDigit(index, length))
            return true;
        else
            return false;
    }

    public static boolean isFirstDigit(int index, int length) {
        if (index == length - 1)
            return true;
        else
            return false;
    }

    public static boolean isLastDigit(int index) {
        if (index == 0)
            return true;
        else
            return false;
    }

    private static int getNumericalValue(String romanLetter) {
        int value = 0;
        switch (romanLetter) {
            case "I":
                value = 1;
                break;
            case "V":
                value = 5;
                break;
            case "X":
                value = 10;
                break;
            case "L":
                value = 50;
                break;
            case "C":
                value = 100;
                break;
            case "D":
                value = 500;
                break;
            case "M":
                value = 1000;
                break;
        }
        return value;
    }
}
