package com.innozones.mms;

public class ArabicToRoman {

    public static String convertArabicNumberToRomanNumber(int number){
        String romanNumber = "";
        if(number < 11){
            romanNumber =  getRomanUnits(number);
        }
        else if(number > 10 && number < 100){
            int units = getNumberUnits(number);
            int tens = getNumberTens(number);

            romanNumber = getRomanTens(tens);
            romanNumber = romanNumber + getRomanUnits(units);
        }
        else if(number > 99 && number < 1000){
            int units = getNumberUnits(number);
            int tens = getNumberTens(number);
            int hundred = getNumberHundreds(number);

            romanNumber = getRomanHundreds(hundred);
            romanNumber = romanNumber + getRomanTens(tens);
            romanNumber = romanNumber + getRomanUnits(units);
        }
        else if(number > 999 && number < 40000){
            int units = getNumberUnits(number);
            int tens = getNumberTens(number);
            int hundred = getNumberHundreds(number);
            int thousands = getNumberThousands(number);

            romanNumber = getRomanThousands(thousands);
            romanNumber = romanNumber + getRomanHundreds(hundred);
            romanNumber = romanNumber + getRomanTens(tens);
            romanNumber = romanNumber + getRomanUnits(units);
        }
        else
            romanNumber = "not covered in this code";
        return romanNumber;
    }

    public static int getNumberUnits(int number){
        int tens = number / 10;
        int units = number - (tens * 10);
        return units;
    }

    public static int getNumberTens(int number){
        int tensAndUnits = number % 100;
        int units = number % 10;
        int tens = (tensAndUnits - units)/10;
        return tens;
    }

    public static int getNumberHundreds(int number){
        int hundredsAndTensAndUnits = number % 1000;
        int hundreds = hundredsAndTensAndUnits/100;
        return hundreds;
    }

    public static int getNumberThousands(int number){
        int thousends = number/1000;
        return thousends;
    }

    private static String getRomanThousands(int number){
        String romanThousands = "";
        switch (number){
            case 1:
                romanThousands = "M";
                break;
            case 2:
                romanThousands = "MM";
                break;
            case 3:
                romanThousands = "MMM";
                break;
        }
        return romanThousands;
    }

    private static String getRomanHundreds(int number){
        String romanHundreds = "";
        switch (number){
            case 1:
                romanHundreds = "C";
                break;
            case 2:
                romanHundreds = "CC";
                break;
            case 3:
                romanHundreds = "CCC";
                break;
            case 4:
                romanHundreds = "CD";
                break;
            case 5:
                romanHundreds = "D";
                break;
            case 6:
                romanHundreds = "DC";
                break;
            case 7:
                romanHundreds = "DCC";
                break;
            case 8:
                romanHundreds = "DCCC";
                break;
            case 9:
                romanHundreds = "CM";
                break;
        }
        return romanHundreds;
    }

    private static String getRomanTens(int number){
        String romanTens = "";
        switch (number){
            case 1:
                romanTens = "X";
                break;
            case 2:
                romanTens = "XX";
                break;
            case 3:
                romanTens = "XXX";
                break;
            case 4:
                romanTens = "XL";
                break;
            case 5:
                romanTens = "L";
                break;
            case 6:
                romanTens = "LX";
                break;
            case 7:
                romanTens = "LXX";
                break;
            case 8:
                romanTens = "LXXX";
                break;
            case 9:
                romanTens = "XC";
                break;
        }
        return romanTens;
    }

    private static String getRomanUnits(int number) {
        String romanNumber = "";
        switch (number){
            case 1:
                romanNumber = "I";
            break;
            case 2:
                romanNumber = "II";
                break;
            case 3:
                romanNumber = "III";
                break;
            case 4:
                romanNumber = "IV";
                break;
            case 5:
                romanNumber = "V";
                break;
            case 6:
                romanNumber = "VI";
                break;
            case 7:
                romanNumber = "VII";
                break;
            case 8:
                romanNumber = "VIII";
                break;
            case 9: romanNumber = "IX";
                break;
            case 10:
                romanNumber = "X";
                break;
        }
        return romanNumber;
    }
}
