package com.innozones.mms;

import org.junit.Test;

import static org.junit.Assert.*;

public class ArabicToRomanTest {
    @Test
    public void RomanFrom1To10(){
        assertEquals("I", ArabicToRoman.convertArabicNumberToRomanNumber(1));
        assertEquals("II", ArabicToRoman.convertArabicNumberToRomanNumber(2));
        assertEquals("III", ArabicToRoman.convertArabicNumberToRomanNumber(3));
        assertEquals("IV", ArabicToRoman.convertArabicNumberToRomanNumber(4));
        assertEquals("V", ArabicToRoman.convertArabicNumberToRomanNumber(5));

        assertEquals("VI", ArabicToRoman.convertArabicNumberToRomanNumber(6));
        assertEquals("VII", ArabicToRoman.convertArabicNumberToRomanNumber(7));
        assertEquals("VIII", ArabicToRoman.convertArabicNumberToRomanNumber(8));
        assertEquals("IX", ArabicToRoman.convertArabicNumberToRomanNumber(9));
        assertEquals("X", ArabicToRoman.convertArabicNumberToRomanNumber(10));
    }

    @Test
    public void RomanFrom11To99(){
        assertEquals("XI", ArabicToRoman.convertArabicNumberToRomanNumber(11));
        assertEquals("XII", ArabicToRoman.convertArabicNumberToRomanNumber(12));
        assertEquals("XIII", ArabicToRoman.convertArabicNumberToRomanNumber(13));
        assertEquals("XIV", ArabicToRoman.convertArabicNumberToRomanNumber(14));
        assertEquals("XVII", ArabicToRoman.convertArabicNumberToRomanNumber(17));

        assertEquals("XX", ArabicToRoman.convertArabicNumberToRomanNumber(20));
        assertEquals("XXVIII", ArabicToRoman.convertArabicNumberToRomanNumber(28));

        assertEquals("XXXI", ArabicToRoman.convertArabicNumberToRomanNumber(31));
        assertEquals("XLIV", ArabicToRoman.convertArabicNumberToRomanNumber(44));

        assertEquals("LVII", ArabicToRoman.convertArabicNumberToRomanNumber(57));
        assertEquals("LXXV", ArabicToRoman.convertArabicNumberToRomanNumber(75));
        assertEquals("LXXXVI", ArabicToRoman.convertArabicNumberToRomanNumber(86));
        assertEquals("XCIX", ArabicToRoman.convertArabicNumberToRomanNumber(99));
    }

    @Test
    public void RomanFrom100To999(){
        assertEquals("C", ArabicToRoman.convertArabicNumberToRomanNumber(100));
        assertEquals("CI", ArabicToRoman.convertArabicNumberToRomanNumber(101));
        assertEquals("CXXIV", ArabicToRoman.convertArabicNumberToRomanNumber(124));
        assertEquals("CLV", ArabicToRoman.convertArabicNumberToRomanNumber(155));
        assertEquals("CLXVII", ArabicToRoman.convertArabicNumberToRomanNumber(167));
        assertEquals("CLXXVIII", ArabicToRoman.convertArabicNumberToRomanNumber(178));
        assertEquals("CLXXXIX", ArabicToRoman.convertArabicNumberToRomanNumber(189));
        assertEquals("CXCIX", ArabicToRoman.convertArabicNumberToRomanNumber(199));
        assertEquals("CC", ArabicToRoman.convertArabicNumberToRomanNumber(200));
        assertEquals("CCI", ArabicToRoman.convertArabicNumberToRomanNumber(201));
        assertEquals("CCV", ArabicToRoman.convertArabicNumberToRomanNumber(205));
        assertEquals("CCIX", ArabicToRoman.convertArabicNumberToRomanNumber(209));
        assertEquals("CCXCVIII", ArabicToRoman.convertArabicNumberToRomanNumber(298));
        assertEquals("CCCXV", ArabicToRoman.convertArabicNumberToRomanNumber(315));
        assertEquals("CDLIII", ArabicToRoman.convertArabicNumberToRomanNumber(453));
        assertEquals("CDLXXXVI", ArabicToRoman.convertArabicNumberToRomanNumber(486));
        assertEquals("DIX", ArabicToRoman.convertArabicNumberToRomanNumber(509));
        assertEquals("DLXXIII", ArabicToRoman.convertArabicNumberToRomanNumber(573));
        assertEquals("DLXXXVIII", ArabicToRoman.convertArabicNumberToRomanNumber(588));
        assertEquals("DXX", ArabicToRoman.convertArabicNumberToRomanNumber(520));
        assertEquals("DCCCXL", ArabicToRoman.convertArabicNumberToRomanNumber(840));
        assertEquals("CMXCIX", ArabicToRoman.convertArabicNumberToRomanNumber(999));
    }

    @Test
    public void RomanFrom1000To3999(){
        assertEquals("M", ArabicToRoman.convertArabicNumberToRomanNumber(1000));
        assertEquals("MX", ArabicToRoman.convertArabicNumberToRomanNumber(1010));
        assertEquals("MVIII", ArabicToRoman.convertArabicNumberToRomanNumber(1008));
        assertEquals("MMXLVI", ArabicToRoman.convertArabicNumberToRomanNumber(2046));
    }

    @Test
    public void testNumberUnits(){
        int units = ArabicToRoman.getNumberUnits(12);
        assertEquals(2, units);
    }

    @Test
    public void testNumberTens(){
        int tens = ArabicToRoman.getNumberTens(12);
        assertEquals(1, tens);
    }

    @Test
    public void test3digit(){
        String x = ArabicToRoman.convertArabicNumberToRomanNumber(124);
        assertEquals("CXXIV", x);
    }

    @Test
    public void test4digit(){
        String x = ArabicToRoman.convertArabicNumberToRomanNumber(1234);
        assertEquals("MCCXXXIV", x);
    }


}