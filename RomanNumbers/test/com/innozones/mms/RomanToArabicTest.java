package com.innozones.mms;

import org.junit.Test;

import static org.junit.Assert.*;

public class RomanToArabicTest {
    @Test
    public void ArabicFrom1To10(){
        assertEquals(1, RomanToArabic.convertRomanNumberToArabicNumber("I"));
        assertEquals(2, RomanToArabic.convertRomanNumberToArabicNumber("II"));
        assertEquals(3, RomanToArabic.convertRomanNumberToArabicNumber("III"));
        assertEquals(4, RomanToArabic.convertRomanNumberToArabicNumber("IV"));
        assertEquals(5, RomanToArabic.convertRomanNumberToArabicNumber("V"));
        assertEquals(6, RomanToArabic.convertRomanNumberToArabicNumber("VI"));

        assertEquals(7, RomanToArabic.convertRomanNumberToArabicNumber("VII"));
        assertEquals(8, RomanToArabic.convertRomanNumberToArabicNumber("VIII"));
        assertEquals(9, RomanToArabic.convertRomanNumberToArabicNumber("IX"));
        assertEquals(10, RomanToArabic.convertRomanNumberToArabicNumber("X"));
    }

    @Test
    public void ArabicFrom11To99(){
        assertEquals(11, RomanToArabic.convertRomanNumberToArabicNumber("XI"));
        assertEquals(12, RomanToArabic.convertRomanNumberToArabicNumber("XII"));
        assertEquals(13, RomanToArabic.convertRomanNumberToArabicNumber("XIII"));
        assertEquals(14, RomanToArabic.convertRomanNumberToArabicNumber("XIV"));
        assertEquals(15, RomanToArabic.convertRomanNumberToArabicNumber("XV"));
        assertEquals(16, RomanToArabic.convertRomanNumberToArabicNumber("XVI"));
        assertEquals(17, RomanToArabic.convertRomanNumberToArabicNumber("XVII"));
        assertEquals(18, RomanToArabic.convertRomanNumberToArabicNumber("XVIII"));
        assertEquals(19, RomanToArabic.convertRomanNumberToArabicNumber("XIX"));
        assertEquals(20, RomanToArabic.convertRomanNumberToArabicNumber("XX"));
        assertEquals(21, RomanToArabic.convertRomanNumberToArabicNumber("XXI"));
        assertEquals(22, RomanToArabic.convertRomanNumberToArabicNumber("XXII"));
        assertEquals(23, RomanToArabic.convertRomanNumberToArabicNumber("XXIII"));
        assertEquals(24, RomanToArabic.convertRomanNumberToArabicNumber("XXIV"));
        assertEquals(25, RomanToArabic.convertRomanNumberToArabicNumber("XXV"));
        assertEquals(26, RomanToArabic.convertRomanNumberToArabicNumber("XXVI"));
        assertEquals(27, RomanToArabic.convertRomanNumberToArabicNumber("XXVII"));
        assertEquals(28, RomanToArabic.convertRomanNumberToArabicNumber("XXVIII"));
        assertEquals(29, RomanToArabic.convertRomanNumberToArabicNumber("XXIX"));
        assertEquals(30, RomanToArabic.convertRomanNumberToArabicNumber("XXX"));
        assertEquals(31, RomanToArabic.convertRomanNumberToArabicNumber("XXXI"));
        assertEquals(32, RomanToArabic.convertRomanNumberToArabicNumber("XXXII"));
        assertEquals(33, RomanToArabic.convertRomanNumberToArabicNumber("XXXIII"));
        assertEquals(34, RomanToArabic.convertRomanNumberToArabicNumber("XXXIV"));
        assertEquals(35, RomanToArabic.convertRomanNumberToArabicNumber("XXXV"));
        assertEquals(36, RomanToArabic.convertRomanNumberToArabicNumber("XXXVI"));
        assertEquals(37, RomanToArabic.convertRomanNumberToArabicNumber("XXXVII"));
        assertEquals(38, RomanToArabic.convertRomanNumberToArabicNumber("XXXVIII"));
        assertEquals(39, RomanToArabic.convertRomanNumberToArabicNumber("XXXIX"));
        assertEquals(40, RomanToArabic.convertRomanNumberToArabicNumber("XL"));
        assertEquals(41, RomanToArabic.convertRomanNumberToArabicNumber("XLI"));
        assertEquals(42, RomanToArabic.convertRomanNumberToArabicNumber("XLII"));
        assertEquals(43, RomanToArabic.convertRomanNumberToArabicNumber("XLIII"));
        assertEquals(44, RomanToArabic.convertRomanNumberToArabicNumber("XLIV"));
        assertEquals(45, RomanToArabic.convertRomanNumberToArabicNumber("XLV"));
        assertEquals(46, RomanToArabic.convertRomanNumberToArabicNumber("XLVI"));
        assertEquals(47, RomanToArabic.convertRomanNumberToArabicNumber("XLVII"));
        assertEquals(48, RomanToArabic.convertRomanNumberToArabicNumber("XLVIII"));
        assertEquals(49, RomanToArabic.convertRomanNumberToArabicNumber("XLIX"));
        assertEquals(50, RomanToArabic.convertRomanNumberToArabicNumber("L"));
        assertEquals(51, RomanToArabic.convertRomanNumberToArabicNumber("LI"));
        assertEquals(52, RomanToArabic.convertRomanNumberToArabicNumber("LII"));
        assertEquals(53, RomanToArabic.convertRomanNumberToArabicNumber("LIII"));
        assertEquals(54, RomanToArabic.convertRomanNumberToArabicNumber("LIV"));
        assertEquals(55, RomanToArabic.convertRomanNumberToArabicNumber("LV"));
        assertEquals(56, RomanToArabic.convertRomanNumberToArabicNumber("LVI"));
        assertEquals(57, RomanToArabic.convertRomanNumberToArabicNumber("LVII"));
        assertEquals(58, RomanToArabic.convertRomanNumberToArabicNumber("LVIII"));
        assertEquals(59, RomanToArabic.convertRomanNumberToArabicNumber("LIX"));
        assertEquals(60, RomanToArabic.convertRomanNumberToArabicNumber("LX"));
        assertEquals(61, RomanToArabic.convertRomanNumberToArabicNumber("LXI"));
        assertEquals(62, RomanToArabic.convertRomanNumberToArabicNumber("LXII"));
        assertEquals(63, RomanToArabic.convertRomanNumberToArabicNumber("LXIII"));
        assertEquals(64, RomanToArabic.convertRomanNumberToArabicNumber("LXIV"));
        assertEquals(65, RomanToArabic.convertRomanNumberToArabicNumber("LXV"));
        assertEquals(66, RomanToArabic.convertRomanNumberToArabicNumber("LXVI"));
        assertEquals(67, RomanToArabic.convertRomanNumberToArabicNumber("LXVII"));
        assertEquals(68, RomanToArabic.convertRomanNumberToArabicNumber("LXVIII"));
        assertEquals(69, RomanToArabic.convertRomanNumberToArabicNumber("LXIX"));
        assertEquals(70, RomanToArabic.convertRomanNumberToArabicNumber("LXX"));
        assertEquals(71, RomanToArabic.convertRomanNumberToArabicNumber("LXXI"));
        assertEquals(72, RomanToArabic.convertRomanNumberToArabicNumber("LXXII"));
        assertEquals(73, RomanToArabic.convertRomanNumberToArabicNumber("LXXIII"));
        assertEquals(74, RomanToArabic.convertRomanNumberToArabicNumber("LXXIV"));
        assertEquals(75, RomanToArabic.convertRomanNumberToArabicNumber("LXXV"));
        assertEquals(76, RomanToArabic.convertRomanNumberToArabicNumber("LXXVI"));
        assertEquals(77, RomanToArabic.convertRomanNumberToArabicNumber("LXXVII"));
        assertEquals(78, RomanToArabic.convertRomanNumberToArabicNumber("LXXVIII"));
        assertEquals(79, RomanToArabic.convertRomanNumberToArabicNumber("LXXIX"));
        assertEquals(80, RomanToArabic.convertRomanNumberToArabicNumber("LXXX"));
        assertEquals(81, RomanToArabic.convertRomanNumberToArabicNumber("LXXXI"));
        assertEquals(82, RomanToArabic.convertRomanNumberToArabicNumber("LXXXII"));
        assertEquals(83, RomanToArabic.convertRomanNumberToArabicNumber("LXXXIII"));
        assertEquals(84, RomanToArabic.convertRomanNumberToArabicNumber("LXXXIV"));
        assertEquals(85, RomanToArabic.convertRomanNumberToArabicNumber("LXXXV"));
        assertEquals(86, RomanToArabic.convertRomanNumberToArabicNumber("LXXXVI"));
        assertEquals(87, RomanToArabic.convertRomanNumberToArabicNumber("LXXXVII"));
        assertEquals(88, RomanToArabic.convertRomanNumberToArabicNumber("LXXXVIII"));
        assertEquals(89, RomanToArabic.convertRomanNumberToArabicNumber("LXXXIX"));
        assertEquals(90, RomanToArabic.convertRomanNumberToArabicNumber("XC"));
        assertEquals(91, RomanToArabic.convertRomanNumberToArabicNumber("XCI"));
        assertEquals(92, RomanToArabic.convertRomanNumberToArabicNumber("XCII"));
        assertEquals(93, RomanToArabic.convertRomanNumberToArabicNumber("XCIII"));
        assertEquals(94, RomanToArabic.convertRomanNumberToArabicNumber("XCIV"));
        assertEquals(95, RomanToArabic.convertRomanNumberToArabicNumber("XCV"));
        assertEquals(96, RomanToArabic.convertRomanNumberToArabicNumber("XCVI"));
        assertEquals(97, RomanToArabic.convertRomanNumberToArabicNumber("XCVII"));
        assertEquals(98, RomanToArabic.convertRomanNumberToArabicNumber("XCVIII" ));
        assertEquals(99, RomanToArabic.convertRomanNumberToArabicNumber("XCIX"));
    }

    @Test
    public void ArabicFrom100To999(){
        assertEquals(100, RomanToArabic.convertRomanNumberToArabicNumber("C"));
        assertEquals(200, RomanToArabic.convertRomanNumberToArabicNumber("CC"));
        assertEquals(300, RomanToArabic.convertRomanNumberToArabicNumber("CCC"));
        assertEquals(400, RomanToArabic.convertRomanNumberToArabicNumber("CD"));
        assertEquals(500, RomanToArabic.convertRomanNumberToArabicNumber("D"));
        assertEquals(600, RomanToArabic.convertRomanNumberToArabicNumber("DC"));
        assertEquals(700, RomanToArabic.convertRomanNumberToArabicNumber("DCC"));
        assertEquals(800, RomanToArabic.convertRomanNumberToArabicNumber("DCCC"));

        assertEquals(900, RomanToArabic.convertRomanNumberToArabicNumber("CM"));
        assertEquals(901, RomanToArabic.convertRomanNumberToArabicNumber("CMI"));
        assertEquals(902, RomanToArabic.convertRomanNumberToArabicNumber("CMII"));
        assertEquals(903, RomanToArabic.convertRomanNumberToArabicNumber("CMIII"));
        assertEquals(904, RomanToArabic.convertRomanNumberToArabicNumber("CMIV"));
        assertEquals(905, RomanToArabic.convertRomanNumberToArabicNumber("CMV"));
        assertEquals(906, RomanToArabic.convertRomanNumberToArabicNumber("CMVI"));
        assertEquals(907, RomanToArabic.convertRomanNumberToArabicNumber("CMVII"));
        assertEquals(908, RomanToArabic.convertRomanNumberToArabicNumber("CMVIII"));
        assertEquals(909, RomanToArabic.convertRomanNumberToArabicNumber("CMIX"));
        assertEquals(910, RomanToArabic.convertRomanNumberToArabicNumber("CMX"));
        assertEquals(911, RomanToArabic.convertRomanNumberToArabicNumber("CMXI"));
        assertEquals(912, RomanToArabic.convertRomanNumberToArabicNumber("CMXII"));
        assertEquals(913, RomanToArabic.convertRomanNumberToArabicNumber("CMXIII"));
        assertEquals(914, RomanToArabic.convertRomanNumberToArabicNumber("CMXIV"));
        assertEquals(915, RomanToArabic.convertRomanNumberToArabicNumber("CMXV"));
        assertEquals(916, RomanToArabic.convertRomanNumberToArabicNumber("CMXVI"));
        assertEquals(917, RomanToArabic.convertRomanNumberToArabicNumber("CMXVII"));
        assertEquals(918, RomanToArabic.convertRomanNumberToArabicNumber("CMXVIII"));
        assertEquals(919, RomanToArabic.convertRomanNumberToArabicNumber("CMXIX"));
        assertEquals(920, RomanToArabic.convertRomanNumberToArabicNumber("CMXX"));
        assertEquals(921, RomanToArabic.convertRomanNumberToArabicNumber("CMXXI"));
        assertEquals(922, RomanToArabic.convertRomanNumberToArabicNumber("CMXXII"));
        assertEquals(923, RomanToArabic.convertRomanNumberToArabicNumber("CMXXIII"));
        assertEquals(924, RomanToArabic.convertRomanNumberToArabicNumber("CMXXIV"));
        assertEquals(925, RomanToArabic.convertRomanNumberToArabicNumber("CMXXV"));
        assertEquals(926, RomanToArabic.convertRomanNumberToArabicNumber("CMXXVI"));
        assertEquals(927, RomanToArabic.convertRomanNumberToArabicNumber("CMXXVII"));
        assertEquals(928, RomanToArabic.convertRomanNumberToArabicNumber("CMXXVIII"));
        assertEquals(929, RomanToArabic.convertRomanNumberToArabicNumber("CMXXIX"));
        assertEquals(930, RomanToArabic.convertRomanNumberToArabicNumber("CMXXX"));
        assertEquals(931, RomanToArabic.convertRomanNumberToArabicNumber("CMXXXI"));
        assertEquals(932, RomanToArabic.convertRomanNumberToArabicNumber("CMXXXII"));
        assertEquals(933, RomanToArabic.convertRomanNumberToArabicNumber("CMXXXIII"));
        assertEquals(934, RomanToArabic.convertRomanNumberToArabicNumber("CMXXXIV"));
        assertEquals(935, RomanToArabic.convertRomanNumberToArabicNumber("CMXXXV"));
        assertEquals(936, RomanToArabic.convertRomanNumberToArabicNumber("CMXXXVI"));
        assertEquals(937, RomanToArabic.convertRomanNumberToArabicNumber("CMXXXVII"));
        assertEquals(938, RomanToArabic.convertRomanNumberToArabicNumber("CMXXXVIII"));
        assertEquals(939, RomanToArabic.convertRomanNumberToArabicNumber("CMXXXIX"));
        assertEquals(940, RomanToArabic.convertRomanNumberToArabicNumber("CMXL"));
        assertEquals(941, RomanToArabic.convertRomanNumberToArabicNumber("CMXLI"));
        assertEquals(942, RomanToArabic.convertRomanNumberToArabicNumber("CMXLII"));
        assertEquals(943, RomanToArabic.convertRomanNumberToArabicNumber("CMXLIII"));
        assertEquals(944, RomanToArabic.convertRomanNumberToArabicNumber("CMXLIV"));
        assertEquals(945, RomanToArabic.convertRomanNumberToArabicNumber("CMXLV"));
        assertEquals(946, RomanToArabic.convertRomanNumberToArabicNumber("CMXLVI"));
        assertEquals(947, RomanToArabic.convertRomanNumberToArabicNumber("CMXLVII"));
        assertEquals(948, RomanToArabic.convertRomanNumberToArabicNumber("CMXLVIII"));
        assertEquals(949, RomanToArabic.convertRomanNumberToArabicNumber("CMXLIX"));
        assertEquals(950, RomanToArabic.convertRomanNumberToArabicNumber("CML"));
        assertEquals(951, RomanToArabic.convertRomanNumberToArabicNumber("CMLI"));
        assertEquals(952, RomanToArabic.convertRomanNumberToArabicNumber("CMLII"));
        assertEquals(953, RomanToArabic.convertRomanNumberToArabicNumber("CMLIII"));
        assertEquals(954, RomanToArabic.convertRomanNumberToArabicNumber("CMLIV"));
        assertEquals(955, RomanToArabic.convertRomanNumberToArabicNumber("CMLV"));
        assertEquals(956, RomanToArabic.convertRomanNumberToArabicNumber("CMLVI"));
        assertEquals(957, RomanToArabic.convertRomanNumberToArabicNumber("CMLVII"));
        assertEquals(958, RomanToArabic.convertRomanNumberToArabicNumber("CMLVIII"));
        assertEquals(959, RomanToArabic.convertRomanNumberToArabicNumber("CMLIX"));
        assertEquals(960, RomanToArabic.convertRomanNumberToArabicNumber("CMLX"));
        assertEquals(961, RomanToArabic.convertRomanNumberToArabicNumber("CMLXI"));
        assertEquals(962, RomanToArabic.convertRomanNumberToArabicNumber("CMLXII"));
        assertEquals(963, RomanToArabic.convertRomanNumberToArabicNumber("CMLXIII"));
        assertEquals(964, RomanToArabic.convertRomanNumberToArabicNumber("CMLXIV"));
        assertEquals(965, RomanToArabic.convertRomanNumberToArabicNumber("CMLXV"));
        assertEquals(966, RomanToArabic.convertRomanNumberToArabicNumber("CMLXVI"));
        assertEquals(967, RomanToArabic.convertRomanNumberToArabicNumber("CMLXVII"));
        assertEquals(968, RomanToArabic.convertRomanNumberToArabicNumber("CMLXVIII"));
        assertEquals(969, RomanToArabic.convertRomanNumberToArabicNumber("CMLXIX"));
        assertEquals(970, RomanToArabic.convertRomanNumberToArabicNumber("CMLXX"));
        assertEquals(971, RomanToArabic.convertRomanNumberToArabicNumber("CMLXXI"));
        assertEquals(972, RomanToArabic.convertRomanNumberToArabicNumber("CMLXXII"));
        assertEquals(973, RomanToArabic.convertRomanNumberToArabicNumber("CMLXXIII"));
        assertEquals(974, RomanToArabic.convertRomanNumberToArabicNumber("CMLXXIV"));
        assertEquals(975, RomanToArabic.convertRomanNumberToArabicNumber("CMLXXV"));
        assertEquals(976, RomanToArabic.convertRomanNumberToArabicNumber("CMLXXVI"));
        assertEquals(977, RomanToArabic.convertRomanNumberToArabicNumber("CMLXXVII"));
        assertEquals(978, RomanToArabic.convertRomanNumberToArabicNumber("CMLXXVIII"));
        assertEquals(979, RomanToArabic.convertRomanNumberToArabicNumber("CMLXXIX"));
        assertEquals(980, RomanToArabic.convertRomanNumberToArabicNumber("CMLXXX"));
        assertEquals(981, RomanToArabic.convertRomanNumberToArabicNumber("CMLXXXI"));
        assertEquals(982, RomanToArabic.convertRomanNumberToArabicNumber("CMLXXXII"));
        assertEquals(983, RomanToArabic.convertRomanNumberToArabicNumber("CMLXXXIII"));
        assertEquals(984, RomanToArabic.convertRomanNumberToArabicNumber("CMLXXXIV"));
        assertEquals(985, RomanToArabic.convertRomanNumberToArabicNumber("CMLXXXV"));
        assertEquals(986, RomanToArabic.convertRomanNumberToArabicNumber("CMLXXXVI"));
        assertEquals(987, RomanToArabic.convertRomanNumberToArabicNumber("CMLXXXVII"));
        assertEquals(988, RomanToArabic.convertRomanNumberToArabicNumber("CMLXXXVIII"));
        assertEquals(989, RomanToArabic.convertRomanNumberToArabicNumber("CMLXXXIX"));
        assertEquals(990, RomanToArabic.convertRomanNumberToArabicNumber("CMXC"));
        assertEquals(991, RomanToArabic.convertRomanNumberToArabicNumber("CMXCI"));
        assertEquals(992, RomanToArabic.convertRomanNumberToArabicNumber("CMXCII"));
        assertEquals(993, RomanToArabic.convertRomanNumberToArabicNumber("CMXCIII"));
        assertEquals(994, RomanToArabic.convertRomanNumberToArabicNumber("CMXCIV"));
        assertEquals(995, RomanToArabic.convertRomanNumberToArabicNumber("CMXCV"));
        assertEquals(996, RomanToArabic.convertRomanNumberToArabicNumber("CMXCVI"));
        assertEquals(997, RomanToArabic.convertRomanNumberToArabicNumber("CMXCVII"));
        assertEquals(998, RomanToArabic.convertRomanNumberToArabicNumber("CMXCVIII"));
        assertEquals(999, RomanToArabic.convertRomanNumberToArabicNumber("CMXCIX"));
    }

    @Test
    public void ArabicFrom1000To3999(){
        assertEquals(1000, RomanToArabic.convertRomanNumberToArabicNumber("M"));
        assertEquals(1010, RomanToArabic.convertRomanNumberToArabicNumber("MX"));
        assertEquals(1008, RomanToArabic.convertRomanNumberToArabicNumber("MVIII"));
        assertEquals(2046, RomanToArabic.convertRomanNumberToArabicNumber("MMXLVI"));
    }

    @Test
    public void testisInBetween(){
        assertTrue(RomanToArabic.isInBetween(1,3));
        assertFalse(RomanToArabic.isInBetween(0, 3));
    }

}